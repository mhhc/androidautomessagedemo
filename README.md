# Android Auto Demo App #
This is a demo app for self-learning Android Auto / Automotive OS

![all apps screenshot](https://bitbucket.org/mhhc/androidautomessagedemo/raw/5a58d80a2df947439450064f9b82c573b563cb97/screenshots/Screen%20Shot%202021-04-17%20at%208.53.08%20PM.png)

Modified Android Automotive Message Service Template (non-functional Hello World app)

![AutoMessage screenshot](https://bitbucket.org/mhhc/androidautomessagedemo/raw/5a58d80a2df947439450064f9b82c573b563cb97/screenshots/Screenshot_1618716206.png)

Google UniversalMusicPlayer Sample

![UniversalMusicPlayer screenshot](https://bitbucket.org/mhhc/androidautomessagedemo/raw/5a58d80a2df947439450064f9b82c573b563cb97/screenshots/Screenshot_1618717224.png)

### Reference ###
* doc
    * https://developer.android.com/training/cars
        * Android Auto is a mobile app developed by Google to mirror features of an Android device, such as a smartphone, on a car's dashboard information and entertainment head unit.
        * Android Automotive OS is an Android-based infotainment system that is built into vehicles.
        * Android Auto and Android Automotive OS support the following types of apps: Media, Messaging, and Navigation, parking, and charging apps
    * https://developer.android.com/studio/archive
        * download Android Studio Canary that contains Android Automotive emulator

* samples
    * https://github.com/android/car-samples/tree/main/car_app_library
        * 99% Java code
    * https://github.com/android/car-samples/tree/main/car-lib
        * runs only on AAOS - kotlin code
    * https://github.com/googlesamples/android-UniversalMusicPlayer
        * showcase music player from streaming with user sign-in

* videos
    * How to build Android apps for cars (Google I/O'19) https://www.youtube.com/watch?v=AHHERLwjUGo
    * What's new with Android for cars (Google I/O'19) https://www.youtube.com/watch?v=v3BKLMU-8Hg
    * How to build media apps for cars (Android Dev Summit '19) https://www.youtube.com/watch?v=Ujwy_AoJnZs

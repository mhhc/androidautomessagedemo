package app.demo.automessage

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import app.demo.automessage.databinding.ActivityMainBinding
import app.demo.automessage.util.NotificationHelper
import app.demo.automsg.model.AutoMsgConversation
import app.demo.automsg.model.AutoMsgUser
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        setContentView(binding.root)

        val notificationHelper = NotificationHelper(this)

        with(binding.mainSendMessageButton) {
            setOnClickListener {
                val conversation = AutoMsgConversation(
                    1,
                    "test",
                    listOf(
                        AutoMsgUser(
                        1, "name", Uri.parse("www.google.com")
                    )
                    ).toMutableList(),
                    BitmapFactory.decodeResource(context.resources,
                        R.drawable.ic_launcher_foreground)
                )
                // TODO: https://developer.android.com/training/cars/messaging#messaging
//                notify(this@MainActivity, conversation)


                notificationHelper.sendNotification(
                    R.drawable.ic_launcher_foreground,
                    "test title",
                    "test body",
                )
            }
        }
    }

    private fun notify(context: Context, appConversation: AutoMsgConversation) {
        // Create our Actions and MessagingStyle from the methods defined above.
//        val replyAction = createReplyAction(context, appConversation)
//        val markAsReadAction = createMarkAsReadAction(context, appConversation)
//        val messagingStyle = createMessagingStyle(context, appConversation)

        // Create the notification.
        val notification = NotificationCompat.Builder(context, "default_notification_channel")
            // A required field for the Android UI.
            .setSmallIcon(R.drawable.ic_launcher_foreground)

            // Shown in Android Auto as the conversation image.
            .setLargeIcon(appConversation.icon)

            // Add MessagingStyle.
//            .setStyle(messagingStyle)

            // Add reply action.
//            .addAction(replyAction)

            // Let's say we don't want to show our mark-as-read Action in the
            // notification shade. We can do that by adding it as invisible so it
            // won't appear in Android UI, but still satisfy Android Auto's
            // mark-as-read Action requirement. You're free to add both Actions as
            // visible or invisible. This is just a stylistic choice.
//            .addInvisibleAction(markAsReadAction)

            .build()

        // Post the notification for the user to see.
        val notificationManagerCompat = NotificationManagerCompat.from(context)
        notificationManagerCompat.notify(appConversation.id, notification)
    }

}
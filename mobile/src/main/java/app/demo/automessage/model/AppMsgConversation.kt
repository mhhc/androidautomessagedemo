package app.demo.automsg.model

import android.graphics.Bitmap
import android.net.Uri

data class AutoMsgConversation(
    val id: Int,
    val title: String,
    val recipients: MutableList<AutoMsgUser>,
    val icon: Bitmap
) {
    companion object {
        /** Fetches [AutoMsgConversation] by its [id]. */
        // TODO: fun getById(id: Int): AutoMsgConversation = // ...
    }

    /** Replies to this conversation with the given [message]. */
    fun reply(message: String) {}

    /** Marks this conversation as read. */
    fun markAsRead() {}

    /** Retrieves all unread messages from this conversation. */
    // TODO: fun getUnreadMessages(): List<YourAppMessage> { return /* ... */ }
}

data class AutoMsgUser(val id: Int, val name: String, val icon: Uri)

data class AutoMsgMessage(
    val id: Int,
    val sender: AutoMsgUser,
    val body: String,
    val timeReceived: Long)